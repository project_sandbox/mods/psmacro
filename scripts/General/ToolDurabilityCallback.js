/*
 * ToolDurabilityWarning
 * Notifies you when you are about to break the tool in your main hand
 */

//Global Imports
var PSPlayer;

var init = function()
{
    //Global Imports
    PSPlayer = PSRequire("gld.psmacro.api.PSPlayer").static;

    //Local Imports
    var PSMacroManager = PSRequire("gld.psmacro.api.PSMacroManager").static;

    PSMacroManager.registerItemDurabilityChanged(PSScriptData, "durabilityCallback");
}

var run = function(args)
{

}

//Callbacks
var durabilityCallback = function(args)
{
    var slot = args.get("slot");
    if(slot == null)
    {
        return;
    }

    if(slot != PSPlayer.InventorySlots.MainHand)
    {
        return;
    }

    var item = args.get("item");
    if(item == null)
    {
        return;
    }

    if(item.psGetMaxDamage() == 0)
    {
        return;
    }

    if(item.psGetDamage() + 50 <= item.psGetMaxDamage())
    {
        return;
    }

    PSPlayer.log("Watch out, your tool is about to break");
    PSPlayer.playSound("minecraft:entity.arrow.hit_player", 1, 1);
}

var stop = function()
{

}

var gui = function(args)
{

}