/*
 * Helper file. Opens creates an text input GUI
 */

//Global imports
var PSGUI = PSRequire("gld.psmacro.api.PSGUI").static;
var PSPlayer = PSRequire("gld.psmacro.api.PSPlayer").static;
var Render2D = PSRequire("gld.psmacro.util.render.Render2D").static;
var Dim = PSRequire("gld.psmacro.structures.render.Dim").static;
var Color = PSRequire("gld.psmacro.structures.render.Color").static;

var GLFW = PSRequire("org.lwjgl.glfw.GLFW").static;

var PSGUIScreen = PSGUI.getPSGUIScreen().static;
var PSText = PSGUI.getPSText().static;

//Controls
var InputGUI_input;

//member variables
var InputGUI_headerText = "Please enter your input here:";
var InputGUI_initText = "Input";
var InputGUI_enterCallback = function(){};

//Extending the GUI screen
var InputGUI = Java.extend(PSGUIScreen, {

    psInit: function()
    {
        inputGUI.super$psInit();
        var parDim = inputGUI.super$getDim();
        InputGUI_input = new PSText();
        InputGUI_input.getDim().setDim(parDim.getX() + 40, parDim.getY2() - (Render2D.getFontHeight() + 8 + 20), parDim.getWidth() - 80, Render2D.getFontHeight() + 8);
        InputGUI_input.setText(InputGUI_initText);
        inputGUI.super$addPSElement(InputGUI_input);
        inputGUI.super$setFocusedPSElement(InputGUI_input);
        inputGUI.super$psInitElements();
    },

    psRender: function(mouseX, mouseY, delta)
    {
        inputGUI.super$PSRenderBackground();
        var parDim = inputGUI.super$getDim();
        var dim = new Dim(parDim.getX() + 40, parDim.getY2() - (Render2D.getFontHeight() + 8 + 20 + Render2D.getFontHeight() + 8), parDim.getWidth() - 80, Render2D.getFontHeight() + 8);
        var color = new Color(0x00FFFFFF);
        Render2D.draw(InputGUI_headerText, dim, color);
        inputGUI.super$psRender(mouseX, mouseY, delta);
    },

    psKeyPressed: function(key, scancode, modifiers)
    {
        if(key == GLFW.GLFW_KEY_ENTER)
        {
            PSPlayer.openGUI(null);
            InputGUI_enterCallback();
            return;
        }
        inputGUI.super$psKeyPressed(key, scancode, modifiers);
    }
});

var inputGUI = new InputGUI();