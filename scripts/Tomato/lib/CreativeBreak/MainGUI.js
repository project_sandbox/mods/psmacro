/*
 * Helper file. Opens the main GUI for the WorldEdit macro;
 */

//Global Imports
var PSGUI = PSRequire("gld.psmacro.api.PSGUI").static;
var Render2D = PSRequire("gld.psmacro.util.render.Render2D").static;
var PSMacroManager = PSRequire("gld.psmacro.api.PSMacroManager").static;
var Dim = PSRequire("gld.psmacro.structures.render.Dim").static;
var Color = PSRequire("gld.psmacro.structures.render.Color").static;

PSLoad("../lib/GUI/EditKeyComboGUI.js");

var PSGUIScreen = PSGUI.getPSGUIScreen().static;
var PSButton = PSGUI.getPSButton().static;

//Controls


//Class Variables
var CreativeBreakMainGUI_parent = null;

//Extending the GUI screen
var CreativeBreakMainGUI = Java.extend(PSGUIScreen, {
    psInit: function()
    {
        creativeBreakMainGUI.super$psInit();
        var parDim = creativeBreakMainGUI.super$getDim();

        var doneButton = new PSButton("Done");
        doneButton.getDim().setDim(parDim.getWidth() - 80, parDim.getHeight() - 50, 60, 30);
        doneButton.setClickCallback(function(){
            PSPlayer.openGUI(CreativeBreakMainGUI_parent);
            CreativeBreakMainGUI_parent = null;
        });
        creativeBreakMainGUI.super$addPSElement(doneButton);

        var breakButton = new PSButton("Break key");
        breakButton.getDim().setDim(parDim.getX() + 40, parDim.getY() + 100, parDim.getWidth() - 80, Render2D.getFontHeight() + 8);
        breakButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = breakMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "break";
            EditKeyComboGUI_parent = creativeBreakMainGUI;
            PSPlayer.openGUI(editKeyComboGUI);
        });
        creativeBreakMainGUI.super$addPSElement(breakButton);

        var placeButton = new PSButton("Place key");
        placeButton.getDim().setDim(parDim.getX() + 40, parDim.getY() + 118 + Render2D.getFontHeight(), parDim.getWidth() - 80, Render2D.getFontHeight() + 8);
        placeButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = placeMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "place";
            EditKeyComboGUI_parent = creativeBreakMainGUI;
            PSPlayer.openGUI(editKeyComboGUI);
        });
        creativeBreakMainGUI.super$addPSElement(placeButton);

        creativeBreakMainGUI.super$psInitElements();
    },

    psRender: function(mouseX, mouseY, delta)
    {
        creativeBreakMainGUI.super$PSRenderBackground();
        var parDim = creativeBreakMainGUI.super$getDim();
        var dimTitle = new Dim(parDim.getX() + 20, parDim.getY() + 20, parDim.getWidth() - 80, Render2D.getFontHeight() + 8);
        var dim = new Dim(parDim.getX() + 40, parDim.getY() + 44, parDim.getWidth() - 80, Render2D.getFontHeight() + 8);
        var color = new Color(0x00FFFFFF);

        Render2D.drawRect(new Dim(10,10, parDim.getWidth() - 20, parDim.getHeight() - 20), color, 3.0);
        Render2D.draw("CreativeBreak GUI", dimTitle, color);
        creativeBreakMainGUI.super$psRender(mouseX, mouseY, delta);
    }
});

var creativeBreakMainGUI = new CreativeBreakMainGUI();


var keyCallback = function()
{
    var newCombo = EditKeyComboGUI_PSKeyComboEditor.getKeyCombo();
    switch(EditKeyComboGUI_buttonEnum)
    {
        case "break":
        {
            PSMacroManager.remove(breakMacro);
            breakMacro.setKeyCombo(newCombo);
            PSMacroManager.register(breakMacro);
            break;
        }
        case "place":
        {
            PSMacroManager.remove(placeMacro);
            placeMacro.setKeyCombo(newCombo);
            PSMacroManager.register(placeMacro);
            break;
        }
        default:
        {
            break;
        }
    }
}