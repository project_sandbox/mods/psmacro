/*
 * Helper file. Opens the main GUI for the WorldEdit macro;
 */

//Global Imports
var PSGUI = PSRequire("gld.psmacro.api.PSGUI").static;
var Render2D = PSRequire("gld.psmacro.util.render.Render2D").static;
var PSMacroManager = PSRequire("gld.psmacro.api.PSMacroManager").static;
var Dim = PSRequire("gld.psmacro.structures.render.Dim").static;
var Color = PSRequire("gld.psmacro.structures.render.Color").static;

PSLoad("../lib/GUI/InputGUI.js");
PSLoad("../lib/GUI/EditKeyComboGUI.js");

var PSGUIScreen = PSGUI.getPSGUIScreen().static;
var PSButton = PSGUI.getPSButton().static;
var PSScrollableList = PSGUI.getPSScrollableList().static;

//Controls
var WorldEditMainGUI_EditName;
var WorldEditMainGUI_ScrollList;

//Class variables
var WorldEditMainGUI_parent = null;

//Extending the GUI screen
var WorldEditMainScreen = Java.extend(PSGUIScreen, {

    psInit: function()
    {
        worldEditMainScreen.super$psInit();
        var parDim = worldEditMainScreen.super$getDim();

        WorldEditMainGUI_EditName = new PSButton("Change Name");
        WorldEditMainGUI_EditName.getDim().setDim(parDim.getX2() - 200, parDim.getY() + 40, 150, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_EditName.setClickCallback(function(){
            //Vars defined by the InputGUI library
            InputGUI_headerText = "Your username:";
            InputGUI_initText = ((userName == null) ? "" : userName);
            InputGUI_enterCallback = getNameCallback;
            //Open the GUI
            PSPlayer.openGUI(inputGUI);
        });
        worldEditMainScreen.super$addPSElement(WorldEditMainGUI_EditName);


        var doneButton = new PSButton("Done");
        doneButton.getDim().setDim(parDim.getWidth() - 80, parDim.getHeight() - 50, 60, 30);
        doneButton.setClickCallback(function(){
            PSPlayer.openGUI(WorldEditMainGUI_parent);
            WorldEditMainGUI_parent = null;
        });
        worldEditMainScreen.super$addPSElement(doneButton);


        //Controls;
        WorldEditMainGUI_ScrollList = new PSScrollableList();
        WorldEditMainGUI_ScrollList.getDim().setDim(parDim.getX() + 40, parDim.getY() + 100, parDim.getWidth() - 80, parDim.getY2() - (parDim.getY() + 200));
        
        
        //pos1key
        var pos1keyButton = new PSButton("pos1key");
        pos1keyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(pos1keyButton);
        pos1keyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = pos1Macro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "pos1key";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });
        
        //pos2key
        var pos2keyButton = new PSButton("pos2key");
        pos2keyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(pos2keyButton);
        pos2keyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = pos2Macro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "pos2key";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //setAirkey
        var setAirkeyButton = new PSButton("setAirkey");
        setAirkeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(setAirkeyButton);
        setAirkeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = setAirMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "setAirkey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //setCustomkey
        var setCustomkeyButton = new PSButton("setCustomkey");
        setCustomkeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(setCustomkeyButton);
        setCustomkeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = setCustomMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "setCustomkey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //undokey
        var undokeyButton = new PSButton("undokey");
        undokeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(undokeyButton);
        undokeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = undoMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "undokey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //redokey
        var redokeyButton = new PSButton("redokey");
        redokeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(redokeyButton);
        redokeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = redoMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "redokey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //copykey
        var copykeyButton = new PSButton("copykey");
        copykeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(copykeyButton);
        copykeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = copyMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "copykey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //pastekey
        var pastekeyButton = new PSButton("pastekey");
        pastekeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(pastekeyButton);
        pastekeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = pasteMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "pastekey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //pasteAirkey
        var pasteAirkeyButton = new PSButton("pasteAirkey");
        pasteAirkeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(pasteAirkeyButton);
        pasteAirkeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = pasteAirMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "pasteAirkey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //deselkey
        var deselkeyButton = new PSButton("deselkey");
        deselkeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(deselkeyButton);
        deselkeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = deselMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "deselkey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //rotateClockkey
        var rotateClockkeyButton = new PSButton("rotateClockkey");
        rotateClockkeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(rotateClockkeyButton);
        rotateClockkeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = rotateClockMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "rotateClockkey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //rotateAntiClockkey
        var rotateAntiClockkeyButton = new PSButton("rotateAntiClockkey");
        rotateAntiClockkeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(rotateAntiClockkeyButton);
        rotateAntiClockkeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = rotateAntiClockMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "rotateAntiClockkey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //flipkey
        var flipkeyButton = new PSButton("flipkey");
        flipkeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(flipkeyButton);
        flipkeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = flipMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "flipkey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //flipNorthkey
        var flipNorthkeyButton = new PSButton("flipNorthkey");
        flipNorthkeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(flipNorthkeyButton);
        flipNorthkeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = flipNorthMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "flipNorthkey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //flipEastkey
        var flipEastkeyButton = new PSButton("flipEastkey");
        flipEastkeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(flipEastkeyButton);
        flipEastkeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = flipEastMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "flipEastkey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //saveSchemkey
        var saveSchemkeyButton = new PSButton("saveSchemkey");
        saveSchemkeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(saveSchemkeyButton);
        saveSchemkeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = saveSchemMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "saveSchemkey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });

        //loadSchemkey
        var loadSchemkeyButton = new PSButton("loadSchemkey");
        loadSchemkeyButton.getDim().setDim(0, 0, 0, Render2D.getFontHeight() + 8);
        WorldEditMainGUI_ScrollList.addPSElement(loadSchemkeyButton);
        loadSchemkeyButton.setClickCallback(function() {
            EditKeyComboGUI_keyCombo = loadSchemMacro.getKeyCombo();
            EditKeyComboGUI_callback = keyCallback;
            EditKeyComboGUI_buttonEnum = "loadSchemkey";
            EditKeyComboGUI_parent = worldEditMainScreen;
            PSPlayer.openGUI(editKeyComboGUI);
        });


        worldEditMainScreen.super$addPSElement(WorldEditMainGUI_ScrollList);


        worldEditMainScreen.super$psInitElements();
    },

    psRender: function(mouseX, mouseY, delta)
    {
        worldEditMainScreen.super$PSRenderBackground();
        var parDim = worldEditMainScreen.super$getDim();
        var dimTitle = new Dim(parDim.getX() + 20, parDim.getY() + 20, parDim.getWidth() - 80, Render2D.getFontHeight() + 8);
        var dim = new Dim(parDim.getX() + 40, parDim.getY() + 44, parDim.getWidth() - 80, Render2D.getFontHeight() + 8);
        var color = new Color(0x00FFFFFF);


        Render2D.drawRect(new Dim(10,10, parDim.getWidth() - 20, parDim.getHeight() - 20), color, 3.0);
        Render2D.draw("WorldEdit GUI", dimTitle, color);
        Render2D.draw("You are using the name: " + userName, dim, color);

        worldEditMainScreen.super$psRender(mouseX, mouseY, delta);
    }
});

var worldEditMainScreen = new WorldEditMainScreen();


var keyCallback = function()
{
    var newCombo = EditKeyComboGUI_PSKeyComboEditor.getKeyCombo();
    switch(EditKeyComboGUI_buttonEnum)
    {
        case "pos1key":
        {
            PSMacroManager.remove(pos1Macro);
            pos1Macro.setKeyCombo(newCombo);
            PSMacroManager.register(pos1Macro);
            break;
        }
        case "pos2key":
        {
            PSMacroManager.remove(pos2Macro);
            pos2Macro.setKeyCombo(newCombo);
            PSMacroManager.register(pos2Macro);
            break;
        }
        case "setAirkey":
        {
            PSMacroManager.remove(setAirMacro);
            setAirMacro.setKeyCombo(newCombo);
            PSMacroManager.register(setAirMacro);
            break;
        }
        case "setCustomkey":
        {
            PSMacroManager.remove(setCustomMacro);
            setCustomMacro.setKeyCombo(newCombo);
            PSMacroManager.register(setCustomMacro);
            break;
        }
        case "undokey":
        {
            PSMacroManager.remove(undoMacro);
            undoMacro.setKeyCombo(newCombo);
            PSMacroManager.register(undoMacro);
            break;
        }
        case "redokey":
        {
            PSMacroManager.remove(redoMacro);
            redoMacro.setKeyCombo(newCombo);
            PSMacroManager.register(redoMacro);
            break;
        }
        case "copykey":
        {
            PSMacroManager.remove(copyMacro);
            copyMacro.setKeyCombo(newCombo);
            PSMacroManager.register(copyMacro);
            break;
        }
        case "pastekey":
        {
            PSMacroManager.remove(pasteMacro);
            pasteMacro.setKeyCombo(newCombo);
            PSMacroManager.register(pasteMacro);
            break;
        }
        case "pasteAirkey":
        {
            PSMacroManager.remove(pasteAirMacro);
            pasteAirMacro.setKeyCombo(newCombo);
            PSMacroManager.register(pasteAirMacro);
            break;
        }
        case "deselkey":
        {
            PSMacroManager.remove(deselMacro);
            deselMacro.setKeyCombo(newCombo);
            PSMacroManager.register(deselMacro);
            break;
        }
        case "rotateClockkey":
        {
            PSMacroManager.remove(rotateClockMacro);
            rotateClockMacro.setKeyCombo(newCombo);
            PSMacroManager.register(rotateClockMacro);
            break;
        }
        case "rotateAntiClockkey":
        {
            PSMacroManager.remove(rotateAntiClockMacro);
            rotateAntiClockMacro.setKeyCombo(newCombo);
            PSMacroManager.register(rotateAntiClockMacro);
            break;
        }
        case "flipkey":
        {
            PSMacroManager.remove(flipMacro);
            flipMacro.setKeyCombo(newCombo);
            PSMacroManager.register(flipMacro);
            break;
        }
        case "flipNorthkey":
        {
            PSMacroManager.remove(flipNorthMacro);
            flipNorthMacro.setKeyCombo(newCombo);
            PSMacroManager.register(flipNorthMacro);
            break;
        }
        case "flipEastkey":
        {
            PSMacroManager.remove(flipEastMacro);
            flipEastMacro.setKeyCombo(newCombo);
            PSMacroManager.register(flipEastMacro);
            break;
        }
        case "saveSchemkey":
        {
            PSMacroManager.remove(saveSchemMacro);
            saveSchemMacro.setKeyCombo(newCombo);
            PSMacroManager.register(saveSchemMacro);
            break;
        }
        case "loadSchemkey":
        {
            PSMacroManager.remove(loadSchemMacro);
            loadSchemMacro.setKeyCombo(newCombo);
            PSMacroManager.register(loadSchemMacro);
            break;
        }
    }
}