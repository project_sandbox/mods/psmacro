/*
 * Worldedit Macro
 * 
 * Adds keybinds for often used Worldedit operations
 */

//Global Imports
var PSPlayer;
var PSGUI;
var PSMacroManager;

var Gson;

var Files;
var StandardOpenOption;

//Global loads
PSLoad("../lib/GUI/InputGUI.js");
PSLoad("lib/WorldEdit/MainGui.js");

//Global Variables
var lastSchemName = "SchemName";
var userName = "";


var pos1Macro;
var pos2Macro;
var setAirMacro;
var setCustom;
var undoMacro;
var redoMacro;
var copyMacro;
var pasteMacro;
var pasteAirMacro;
var deselMacro;
var rotateClockMacro;
var rotateAntiClockMacro;
var flipMacro;
var flipNorthMacro;
var flipEastMacro;
var saveSchemMacro;
var loadSchemMacro;

var chatKey;

var init = function()
{
    //Imports
    //Global
    PSPlayer = PSRequire("gld.psmacro.api.PSPlayer").static;
    PSGUI = PSRequire("gld.psmacro.api.PSGUI").static;
    PSMacroManager    = PSRequire("gld.psmacro.api.PSMacroManager").static;

    Gson = PSRequire("com.google.gson.Gson").static;

    Files = PSRequire("java.nio.file.Files").static;
    StandardOpenOption = PSRequire("java.nio.file.StandardOpenOption").static;

    //Local
    var GLFW            = PSRequire("org.lwjgl.glfw.GLFW").static;
    var PSInputUtil     = PSRequire("gld.psmacro.api.PSInputUtil").static;

    //Getting all Keybinds (KP stands for the keypad / numpad)
    var pos1key     = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_1, 0);
    var pos2key     = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_2, 0);
    var setAirkey   = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_0, 0);
    setAirkey.addKeyCodeRequirement(GLFW.GLFW_KEY_LEFT_CONTROL, 0, true);
    var setCustomkey = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_0, 0);
    setCustomkey.addKeyCodeRequirement(GLFW.GLFW_KEY_LEFT_CONTROL, 0, false);
    var undokey     = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_DECIMAL, 0);
    var redokey     = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_3, 0);
    var copykey     = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_4, 0);
    var pastekey    = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_5, 0);
    pastekey.addKeyCodeRequirement(GLFW.GLFW_KEY_LEFT_CONTROL, 0, false);
    var pasteAirkey    = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_5, 0);
    pasteAirkey.addKeyCodeRequirement(GLFW.GLFW_KEY_LEFT_CONTROL, 0, true);
    var deselkey    = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_ENTER, 0);

    var rotateClockkey  = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_7, 0);
    rotateClockkey.addKeyCodeRequirement(GLFW.GLFW_KEY_LEFT_CONTROL, 0, true);
    var rotateAntiClockkey = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_7, 0);
    rotateAntiClockkey.addKeyCodeRequirement(GLFW.GLFW_KEY_LEFT_CONTROL, 0, false);

    var flipkey = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_8, 0);
    flipkey.addKeyCodeRequirement(GLFW.GLFW_KEY_LEFT_CONTROL, 0, true);
    flipkey.addKeyCodeRequirement(GLFW.GLFW_KEY_BACKSLASH, 0, true);
    var flipNorthkey = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_8, 0);
    flipNorthkey.addKeyCodeRequirement(GLFW.GLFW_KEY_LEFT_CONTROL, 0, false);
    flipNorthkey.addKeyCodeRequirement(GLFW.GLFW_KEY_BACKSLASH, 0, false);

    var flipEastkey = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_KP_8, 0);
    flipEastkey.addKeyCodeRequirement(GLFW.GLFW_KEY_LEFT_CONTROL, 0, false);
    flipEastkey.addKeyCodeRequirement(GLFW.GLFW_KEY_BACKSLASH, 0, true);

    var saveSchemKey = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_PAGE_UP, 0);
    var loadSchemKey = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_PAGE_DOWN, 0);


    chatKey = PSInputUtil.getKeyCode(GLFW.GLFW_KEY_T, 0);


    //Load saved data from config
    var dataFile = PSScriptData.getFile("data/WorldEditConfig.json");
    var input = null;
    //Reading data from file
    try
    {
        input = Files.readAllLines(dataFile);
    }
    catch(e)
    {
        //e.printStackTrace();
        //Fail silently. As it mostly means the file hasn't been created yet
    }
    if(input != null)
    {
        //Appending all lines into 1 so it can be parsed by the JSON module
        var inputString = "";
        for(var i = 0; i < input.length; i++)
        {
            inputString += input[i];
        }
        //Convert the string to actual values
        var values = JSON.parse(inputString);
        //If the expected keys are set, store the key value into the data file
        if("lastSchemName" in values)
        {
            lastSchemName = values["lastSchemName"];
        }
        if("userName" in values)
        {
            userName = values["userName"];
        }
        //Load and set the keybinds
        if("keybinds" in values)
        {
            var keybinds = values["keybinds"];
            var gson = new Gson();

            if("pos1" in keybinds)
            {
                var pos1 = keybinds["pos1"];
                pos1key = gson.fromJson(pos1, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("pos2" in keybinds)
            {
                var pos2 = keybinds["pos2"];
                pos2key = gson.fromJson(pos2, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("setAir" in keybinds)
            {
                var setAir = keybinds["setAir"];
                setAirkey = gson.fromJson(setAir, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("setCustom" in keybinds)
            {
                var setCustom = keybinds["setCustom"];
                setCustomkey = gson.fromJson(setCustom, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("undo" in keybinds)
            {
                var undo = keybinds["undo"];
                undokey = gson.fromJson(undo, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("redo" in keybinds)
            {
                var redo = keybinds["redo"];
                redokey = gson.fromJson(redo, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("copy" in keybinds)
            {
                var copy = keybinds["copy"];
                copykey = gson.fromJson(copy, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("paste" in keybinds)
            {
                var paste = keybinds["paste"];
                pastekey = gson.fromJson(paste, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("pasteAir" in keybinds)
            {
                var pasteAir = keybinds["pasteAir"];
                pasteAirkey = gson.fromJson(pasteAir, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("desel" in keybinds)
            {
                var desel = keybinds["desel"];
                deselkey = gson.fromJson(desel, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("rotateClock" in keybinds)
            {
                var rotateClock = keybinds["rotateClock"];
                rotateClockkey = gson.fromJson(rotateClock, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("rotateAntiClock" in keybinds)
            {
                var rotateAntiClock = keybinds["rotateAntiClock"];
                rotateAntiClockkey = gson.fromJson(rotateAntiClock, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("flip" in keybinds)
            {
                var flip = keybinds["flip"];
                flipkey = gson.fromJson(flip, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("flipNorth" in keybinds)
            {
                var flipNorth = keybinds["flipNorth"];
                flipNorthkey = gson.fromJson(flipNorth, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("flipEast" in keybinds)
            {
                var flipEast = keybinds["flipEast"];
                flipEastkey = gson.fromJson(flipEast, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("saveSchem" in keybinds)
            {
                var saveSchem = keybinds["saveSchem"];
                saveSchemkey = gson.fromJson(saveSchem, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
            if("loadSchem" in keybinds)
            {
                var loadSchem = keybinds["loadSchem"];
                loadSchemkey = gson.fromJson(loadSchem, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
        }
    }




    //Creating the macro's
    pos1Macro   = PSMacroManager.newPressMacro(pos1key, PSScriptData);
    pos1Macro.setFunction("pos1");
    pos2Macro   = PSMacroManager.newPressMacro(pos2key, PSScriptData);
    pos2Macro.setFunction("pos2");
    setAirMacro = PSMacroManager.newPressMacro(setAirkey, PSScriptData);
    setAirMacro.setFunction("setAir");
    setCustomMacro = PSMacroManager.newPressMacro(setCustomkey, PSScriptData);
    setCustomMacro.setFunction("setCustom");
    undoMacro   = PSMacroManager.newPressMacro(undokey, PSScriptData);
    undoMacro.setFunction("undo");
    redoMacro   = PSMacroManager.newPressMacro(redokey, PSScriptData);
    redoMacro.setFunction("redo");
    copyMacro   = PSMacroManager.newPressMacro(copykey, PSScriptData);
    copyMacro.setFunction("copy");
    pasteMacro  = PSMacroManager.newPressMacro(pastekey, PSScriptData);
    pasteMacro.setFunction("paste");
    pasteAirMacro  = PSMacroManager.newPressMacro(pasteAirkey, PSScriptData);
    pasteAirMacro.setFunction("pasteAir");
    deselMacro  = PSMacroManager.newPressMacro(deselkey, PSScriptData);
    deselMacro.setFunction("desel");

    rotateClockMacro = PSMacroManager.newPressMacro(rotateClockkey, PSScriptData);
    rotateClockMacro.setFunction("rotateClock");
    rotateAntiClockMacro = PSMacroManager.newPressMacro(rotateAntiClockkey, PSScriptData);
    rotateAntiClockMacro.setFunction("rotateAntiClock");
    flipMacro = PSMacroManager.newPressMacro(flipkey, PSScriptData);
    flipMacro.setFunction("flip");
    flipNorthMacro = PSMacroManager.newPressMacro(flipNorthkey, PSScriptData);
    flipNorthMacro.setFunction("flipNorth");
    flipEastMacro = PSMacroManager.newPressMacro(flipEastkey, PSScriptData);
    flipEastMacro.setFunction("flipEast");

    saveSchemMacro  = PSMacroManager.newPressMacro(saveSchemKey, PSScriptData);
    saveSchemMacro.setFunction("saveSchem");
    loadSchemMacro  = PSMacroManager.newPressMacro(loadSchemKey, PSScriptData);
    loadSchemMacro.setFunction("loadSchem");

    //Registering the macro's
    PSMacroManager.register(pos1Macro);
    PSMacroManager.register(pos2Macro);
    PSMacroManager.register(setAirMacro);
    PSMacroManager.register(setCustomMacro);
    PSMacroManager.register(undoMacro);
    PSMacroManager.register(redoMacro);
    PSMacroManager.register(copyMacro);
    PSMacroManager.register(pasteMacro);
    PSMacroManager.register(pasteAirMacro);
    PSMacroManager.register(deselMacro);

    PSMacroManager.register(rotateClockMacro);
    PSMacroManager.register(rotateAntiClockMacro);
    PSMacroManager.register(flipMacro);
    PSMacroManager.register(flipNorthMacro);
    PSMacroManager.register(flipEastMacro);

    PSMacroManager.register(saveSchemMacro);
    PSMacroManager.register(loadSchemMacro);
}

var run = function(args)
{

}

var pos1 = function(args)
{
    PSPlayer.sendMessage("//pos1");
}

var pos2 = function(args)
{
    PSPlayer.sendMessage("//pos2");
}

var setAir = function(args)
{
    PSPlayer.sendMessage("//set air");
}

var setCustom = function(args)
{
    PSPlayer.pressKey(chatKey);
    PSPlayer.releaseKey(chatKey);

    PSMacroManager.runTaskDelayed(function() {
        PSPlayer.type("//set ");
    }, 1);
}

var undo = function(args)
{
    PSPlayer.sendMessage("//undo");
}

var redo = function(args)
{
    PSPlayer.sendMessage("//redo");
}

var copy = function(args)
{
    PSPlayer.sendMessage("//copy");
}

var paste = function(args)
{
    PSPlayer.sendMessage("//paste");
}

var pasteAir = function(args)
{
    PSPlayer.sendMessage("//paste -a");
}

var desel = function(args)
{
    PSPlayer.sendMessage("//desel");
}

var rotateClock = function(args)
{
    PSPlayer.sendMessage("//rotate 90");
}

var rotateAntiClock = function(args)
{
    PSPlayer.sendMessage("//rotate -90");
}

var flip = function(args)
{
    PSPlayer.sendMessage("//flip");
}

var flipNorth = function(args)
{
    PSPlayer.sendMessage("//flip n");
}

var flipEast = function(args)
{
    PSPlayer.sendMessage("//flip e");
}

var saveSchem = function(args)
{
    if(getUserName())
    {
        return;
    }

    //Vars defined by the InputGUI library
    InputGUI_headerText = "Name of Schematic you want to save:";
    InputGUI_initText = ((lastSchemName == null) ? "" : lastSchemName);
    InputGUI_enterCallback = saveSchemCallback;
    //Open the GUI
    PSPlayer.openGUI(inputGUI);
}

var loadSchem = function(args)
{
    if(getUserName())
    {
        return;
    }

    /*var macroTextInput = PSGUI.getInputGUI(loadSchemCallback);
    macroTextInput.setDefaultText(lastSchemName);
    PSPlayer.openGUI(macroTextInput);*/

    //Vars defined by the InputGUI library
    InputGUI_headerText = "Name of Schematic you want to load:";
    InputGUI_initText = ((lastSchemName == null) ? "" : lastSchemName);
    InputGUI_enterCallback = loadSchemCallback;
    //Open the GUI
    PSPlayer.openGUI(inputGUI);
}


//Helper functions
var getUserName = function()
{
    if(userName == "")
    {
        /*var macroTextInput = PSGUI.getInputGUI(getNameCallback);
        macroTextInput.setDefaultText("Your name");
        PSPlayer.openGUI(macroTextInput);*/
        //Vars defined by the InputGUI library
        InputGUI_headerText = "Your username:";
        InputGUI_initText = ((userName == null) ? "" : userName);
        InputGUI_enterCallback = getNameCallback;
        //Open the GUI
        PSPlayer.openGUI(inputGUI);
        return true;
    }
    return false;
}

//Callback Functions
var getNameCallback = function(args)
{
    var input = InputGUI_input.getString();//args.get("input");
    if(input == null)
    {
        print("Player inputted null");
        return;
    }
    PSPlayer.log("You registered the name: " + input);
    PSPlayer.log("Keep in mind that the schematic didn't save as you had to run the initialization first!");
    userName = input;
}

var saveSchemCallback = function(args)
{
    var schemName = InputGUI_input.getString();//args.get("input");
    if(schemName == null || schemName == "")
    {
        PSPlayer.sendMessage("You need to specify a schematic name");
        return;
    }
    PSPlayer.sendMessage("//schem save " + userName + "_" + schemName);
    lastSchemName = schemName;
}

var loadSchemCallback = function(args)
{
    var schemName = InputGUI_input.getString();//args.get("input");
    if(schemName == null || schemName == "")
    {
        PSPlayer.sendMessage("You need to specify a schematic name");
        return;
    }
    PSPlayer.sendMessage("//schem load " + userName + "_" + schemName);
    lastSchemName = schemName;
}

var stop = function()
{
    //Save Keybinds
    //Use Googles gson library to parse the keycombo's as the library will do the heavy work instead of yourself
    var gson = new Gson();
    var keybinds = {
        "pos1": gson.toJson(PSMacroManager.getKeyComboStorage(pos1Macro.getKeyCombo())),
        "pos2": gson.toJson(PSMacroManager.getKeyComboStorage(pos2Macro.getKeyCombo())),
        "setAir": gson.toJson(PSMacroManager.getKeyComboStorage(setAirMacro.getKeyCombo())),
        "setCustom": gson.toJson(PSMacroManager.getKeyComboStorage(setCustomMacro.getKeyCombo())),
        "undo": gson.toJson(PSMacroManager.getKeyComboStorage(undoMacro.getKeyCombo())),
        "redo": gson.toJson(PSMacroManager.getKeyComboStorage(redoMacro.getKeyCombo())),
        "copy": gson.toJson(PSMacroManager.getKeyComboStorage(copyMacro.getKeyCombo())),
        "paste": gson.toJson(PSMacroManager.getKeyComboStorage(pasteMacro.getKeyCombo())),
        "pasteAir": gson.toJson(PSMacroManager.getKeyComboStorage(pasteAirMacro.getKeyCombo())),
        "desel": gson.toJson(PSMacroManager.getKeyComboStorage(deselMacro.getKeyCombo())),
        "rotateClock": gson.toJson(PSMacroManager.getKeyComboStorage(rotateClockMacro.getKeyCombo())),
        "rotateAntiClock": gson.toJson(PSMacroManager.getKeyComboStorage(rotateAntiClockMacro.getKeyCombo())),
        "flip": gson.toJson(PSMacroManager.getKeyComboStorage(flipMacro.getKeyCombo())),
        "flipNorth": gson.toJson(PSMacroManager.getKeyComboStorage(flipNorthMacro.getKeyCombo())),
        "flipEast": gson.toJson(PSMacroManager.getKeyComboStorage(flipEastMacro.getKeyCombo())),
        "saveSchem": gson.toJson(PSMacroManager.getKeyComboStorage(saveSchemMacro.getKeyCombo())),
        "loadSchem": gson.toJson(PSMacroManager.getKeyComboStorage(loadSchemMacro.getKeyCombo()))
    };

    //Saving data
    var saveData = {"lastSchemName": lastSchemName, "userName": userName, "keybinds": keybinds};

    //Converting data to JSON string
    var saveString = JSON.stringify(saveData);
    
    //Get the data file
    PSScriptData.createFolder("data");
    var dataFile = PSScriptData.getFile("data/WorldEditConfig.json");

    try
    {
        Files.write(dataFile, saveString.getBytes(), StandardOpenOption.CREATE);
    }
    catch(e)
    {
        e.printStackTrace();
    }
}

var gui = function(args)
{
    WorldEditMainGUI_parent = PSPlayer.getGUI();
    PSPlayer.openGUI(worldEditMainScreen);
}