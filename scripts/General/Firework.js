/*
 * Firework
 * Shoots firework when the /firework command exists on the server
 */

//Global Imports
var PSPlayer;

var init = function()
{
    //Global Imports
    PSPlayer = PSRequire("gld.psmacro.api.PSPlayer").static;

    //Local Imports
    var PSMacroManager = PSRequire("gld.psmacro.api.PSMacroManager").static;
    var PSInputUtil = PSRequire("gld.psmacro.api.PSInputUtil").static;
    var GLFW = PSRequire("org.lwjgl.glfw.GLFW").static;

    //Create keyCombos
    var uCombo = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_U, 0);
    uCombo.addKeyCodeRequirement(GLFW.GLFW_KEY_LEFT_CONTROL, 0, true);

    var ctrlUCombo = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_U, 0);
    ctrlUCombo.addKeyCodeRequirement(GLFW.GLFW_KEY_LEFT_CONTROL, 0, false);
    
    //Creating Macros
    var fireworkMacro = PSMacroManager.newPressMacro(uCombo, PSScriptData);

    var ctrlFireworkMacro = PSMacroManager.newPressMacro(ctrlUCombo, PSScriptData);
    ctrlFireworkMacro.setFunction("ctrlFirework");

    //Register macros
    PSMacroManager.register(fireworkMacro);
    PSMacroManager.register(ctrlFireworkMacro);

}

var run = function(args)
{
    PSPlayer.sendMessage("/firework");
    PSPlayer.log("Firework sent into the air. Enjoy the show");
}

var ctrlFirework = function(args)
{
    PSPlayer.log("No, don't press control");
}

var stop = function()
{

}

var gui = function(args)
{

}