/*
 * Creative Break
 */

//Global Imports
var PSPlayer;
var PSMacroManager;
var Gson;
var Files;
var StandardOpenOption;

//Global loads
PSLoad("lib/CreativeBreak/MainGUI.js");

//Global variables
var breakKey;
var placeKey;

var breakMacro;
var placeMacro;

var init = function()
{
    //Global Imports
    PSPlayer = PSRequire("gld.psmacro.api.PSPlayer").static;
    PSMacroManager = PSRequire("gld.psmacro.api.PSMacroManager").static;
    Gson = PSRequire("com.google.gson.Gson").static;
    Files = PSRequire("java.nio.file.Files").static;
    StandardOpenOption = PSRequire("java.nio.file.StandardOpenOption").static;

    //Local imports
    var PSInputUtil = PSRequire("gld.psmacro.api.PSInputUtil").static;
    var GLFW = PSRequire("org.lwjgl.glfw.GLFW").static;

    //getting the keys
    breakKey = PSInputUtil.getMouseButton(GLFW.GLFW_MOUSE_BUTTON_1);
    placeKey = PSInputUtil.getMouseButton(GLFW.GLFW_MOUSE_BUTTON_2);

    var quickBreakKeyCombo = PSInputUtil.getKeyComboMouse(GLFW.GLFW_MOUSE_BUTTON_5);
    var quickPlaceKeyCombo = PSInputUtil.getKeyComboMouse(GLFW.GLFW_MOUSE_BUTTON_4);

    //Load saved ata from config
    var dataFile = PSScriptData.getFile("data/CreativeBreakConfig.json");
    var input = null;

    try
    {
        input = Files.readAllLines(dataFile);
    }
    catch(e)
    {
        //Fail silent as it mostly means the config file doesn't exist yet.
    }
    if(input != null)
    {
        //Appending all lines into 1 so it can be parsed by the Json module
        var inputString = "";
        for(var i = 0; i < input.length; i++)
        {
            inputString += input[i];
        }
        //Convert the string to actual values
        var values = JSON.parse(inputString);
        //if the expected keys are set, store the key value into the variables

        //load and set the keybinds
        if("keybinds" in values)
        {
            var keybinds = values["keybinds"];
            var gson = new Gson();

            if("break" in keybinds)
            {
                var breakVal = keybinds["break"];
                quickBreakKeyCombo = gson.fromJson(breakVal, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }

            if("place" in keybinds)
            {
                var place = keybinds["place"];
                quickPlaceKeyCombo = gson.fromJson(place, PSMacroManager.getKeyComboStorageClass()).getCombo();
            }
        }
    }

    breakMacro = PSMacroManager.newRepeatingMacro(quickBreakKeyCombo, PSScriptData, 1);
    breakMacro.setFunction("breakFunction");

    placeMacro = PSMacroManager.newRepeatingMacro(quickPlaceKeyCombo, PSScriptData, 1);
    placeMacro.setFunction("placeFunction");

    PSMacroManager.register(breakMacro);
    PSMacroManager.register(placeMacro);

}

var run = function(args)
{

}

//Macro Functions
var breakFunction = function(args)
{
    PSPlayer.pressKey(breakKey);
    PSPlayer.releaseKey(breakKey);
}

var placeFunction = function(args)
{
    PSPlayer.pressKey(placeKey);
    PSPlayer.releaseKey(placeKey);
}

var stop = function()
{
    //Save Keybinds
    var gson = new Gson();
    var keybinds = {
        "break": gson.toJson(PSMacroManager.getKeyComboStorage(breakMacro.getKeyCombo())),
        "place": gson.toJson(PSMacroManager.getKeyComboStorage(placeMacro.getKeyCombo()))
    };

    //Saving data
    var saveData = {"keybinds": keybinds};

    //Converting data to Json string
    var saveString = JSON.stringify(saveData);

    //Get the data file
    PSScriptData.createFolder("data");
    var dataFile = PSScriptData.getFile("data/CreativeBreakConfig.json");

    try
    {
        Files.write(dataFile, saveString.getBytes(), StandardOpenOption.CREATE);
    }
    catch(e)
    {
        e.printStackTrace();
    }
}

var gui = function(args)
{
    CreativeBreakMainGUI_parent = PSPlayer.getGUI();
    PSPlayer.openGUI(creativeBreakMainGUI);
}