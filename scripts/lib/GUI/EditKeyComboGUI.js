/*
 * Helper file. Opens creates an text input GUI
 */

//Global imports
var PSGUI = PSRequire("gld.psmacro.api.PSGUI").static;
var PSPlayer = PSRequire("gld.psmacro.api.PSPlayer").static;
var Render2D = PSRequire("gld.psmacro.util.render.Render2D").static;
var Dim = PSRequire("gld.psmacro.structures.render.Dim").static;
var Color = PSRequire("gld.psmacro.structures.render.Color").static;


var PSGUIScreen = PSGUI.getPSGUIScreen().static;
var PSKeyComboEditor = PSGUI.getPSKeyComboEditor().static;
var PSButton = PSGUI.getPSButton().static;

//Controls
var EditKeyComboGUI_PSKeyComboEditor;

//member variables
var EditKeyComboGUI_keyCombo;
var EditKeyComboGUI_callback;
var EditKeyComboGUI_buttonEnum;
var EditKeyComboGUI_parent;

//Extending the GUI screen
var EditKeyComboGUI = Java.extend(PSGUIScreen, {
    psInit: function()
    {
        editKeyComboGUI.super$psInit();
        var parDim = editKeyComboGUI.super$getDim();

        EditKeyComboGUI_PSKeyComboEditor = new PSKeyComboEditor(EditKeyComboGUI_keyCombo);
        EditKeyComboGUI_PSKeyComboEditor.getDim().setDim(parDim.getX() + 40, parDim.getY() + 80, parDim.getWidth() - 80, parDim.getHeight() - 200);
        editKeyComboGUI.super$addPSElement(EditKeyComboGUI_PSKeyComboEditor);

        var changeButton = new PSButton("Change");
        changeButton.getDim().setDim(parDim.getWidth() - 80, parDim.getHeight() - 50, 60, 30);
        changeButton.setClickCallback(function(){
            EditKeyComboGUI_callback();
            PSPlayer.openGUI(EditKeyComboGUI_parent);
        });
        editKeyComboGUI.super$addPSElement(changeButton);

        var cancelButton = new PSButton("Cancel");
        cancelButton.getDim().setDim(parDim.getWidth() - 150, parDim.getHeight() - 50, 60, 30);
        cancelButton.setClickCallback(function(){
            PSPlayer.openGUI(EditKeyComboGUI_parent);
        });
        editKeyComboGUI.super$addPSElement(cancelButton);

        editKeyComboGUI.super$psInitElements();
    },

    psRender: function(mouseX, mouseY, delta)
    {
        editKeyComboGUI.super$PSRenderBackground();
        var parDim = editKeyComboGUI.super$getDim();
        var dimTitle = new Dim(parDim.getX() + 20, parDim.getY() + 20, parDim.getWidth() - 80, Render2D.getFontHeight() + 8);
        var color = new Color(0x00FFFFFF);


        Render2D.drawRect(new Dim(10,10, parDim.getWidth() - 20, parDim.getHeight() - 20), color, 3.0);
        Render2D.draw("KeyEditor: " + EditKeyComboGUI_buttonEnum, dimTitle, color);
        editKeyComboGUI.super$psRender(mouseX, mouseY, delta);
    }

});

var editKeyComboGUI = new EditKeyComboGUI();