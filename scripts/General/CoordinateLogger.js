/*
 * CoordinateLogger
 *
 * Write out player coordinates for easy reporting
 * 
 */

//Global Imports
var PSPlayer;
var PSMacroManager;

//Global Variables
var openChatKey;

var init = function()
{
    //Global Imports
    PSPlayer = PSRequire("gld.psmacro.api.PSPlayer").static;
    PSMacroManager = PSRequire("gld.psmacro.api.PSMacroManager").static;

    //Local imports
    var GLFW = PSRequire("org.lwjgl.glfw.GLFW").static;
    var PSInputUtil = PSRequire("gld.psmacro.api.PSInputUtil").static;

    //Creating the KeyCombo
    var keyCombo = PSInputUtil.getKeyComboKeyCode(GLFW.GLFW_KEY_RIGHT_BRACKET, 0);
    openChatKey = PSInputUtil.getKeyCode(GLFW.GLFW_KEY_T, 0);
    
    //Creating the macro
    var typeMacro = PSMacroManager.newPressMacro(keyCombo, PSScriptData);

    //Registering the macro
    PSMacroManager.register(typeMacro);
}

var run = function(args)
{
    PSPlayer.pressKey(openChatKey);
    PSPlayer.releaseKey(openChatKey);
    PSMacroManager.runTaskDelayed(function() {
        PSPlayer.type((PSPlayer.getX()|0) + " " + (PSPlayer.getY()|0) + " " + (PSPlayer.getZ()|0) + " " + (PSPlayer.getYaw()|0) + " " + (PSPlayer.getPitch()|0));
    }, 1);
}

var stop = function()
{

}

var gui = function(args)
{

}